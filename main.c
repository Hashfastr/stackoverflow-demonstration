#include <stdio.h>
#include <regex.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pwd.h>
#include <termios.h>
#include <spawn.h>
#include <sys/wait.h>

extern char **environ;

/**
 * get_stored_hash
 *
 * @param hash A pointer to the string to store the retrieved hash in
 *
 * @return 0 on failure, 1 on success
 */
int get_stored_hash(char *hash) { 
  // vars for reading in /etc/shadow
  size_t len = 0;
  char *line = NULL;
  FILE *fp = NULL;

  // regex variables
  regmatch_t capture_groups[8];
  regex_t user_details;
  char username_tmp[64], hash_tmp[128];
  int tmp;

  // compile regex
  if (regcomp(&user_details, "([^:]+):(\\$[^:]+)", REG_EXTENDED)) {
    printf("Could not compile user details regex!");
    exit(1);
  }

  // /etc/shadow holds password hashes so open that to retrieve the stored hashes
  if ((fp = fopen("/etc/shadow", "r")) == NULL) {
    printf("Could not open /etc/shadow\nIs the sticky-bit set?\n");
    return 0;
  }

  // Look for our user's hash
  while (getline(&line, &len, fp) != -1) {
    // If we have a match
    if (!regexec(&user_details, line, 8, capture_groups, 0)) {
      // Using the start and stop intervals of the first capture group
      // Extract the username from the line
      tmp = capture_groups[1].rm_eo - capture_groups[1].rm_so;
      strncpy(username_tmp, line + capture_groups[1].rm_so, tmp);
      username_tmp[tmp] = 0;

      // Using the start and stop intervals of the second capture group
      // Extract the password hash from the line
      tmp = capture_groups[2].rm_eo - capture_groups[2].rm_so;
      strncpy(hash_tmp, line + capture_groups[2].rm_so, tmp);
      hash_tmp[tmp] = 0;
 
      // If we found our user, copy their hash into the provided string
      if (!strcmp(getpwuid(getuid())->pw_name, username_tmp)) {
        strcpy(hash, hash_tmp);
       
        // Free resources
        regfree(&user_details);
        free(line);
        fclose(fp);
        return 1;
      }
    }
  }

  // Free resources
  regfree(&user_details);
  free(line);
  fclose(fp);
  return 0;
}

int authenticate(char *password, char *stored_hash) {
  // hash info variables
  char salt[32];

  // regex variables
  regmatch_t match[8];
  regex_t hash_match;
  int tmp;

  // Zero out the salt string
  for (int i = 0; i < 32; i++)
    salt[i] = 0;

  // compile regex
  if (regcomp(&hash_match, "\\$[^\\$]+\\$[^\\$]+\\$", REG_EXTENDED)) {
    printf("Could not compile hash match regex!");
    return 0;
  }

  // Try to match the salt from our retrieved hash
  if (!regexec(&hash_match, stored_hash, 8, match, 0)) {
    // Using the start and stop intervals of the first capture group
    // Extract the username from the line
    tmp = match[0].rm_eo - match[0].rm_so;
    strncpy(salt, stored_hash + match[0].rm_so, tmp);
    salt[tmp] = 0;
   
    // We don't need our hash regex anymore
    regfree(&hash_match);
    if (!strcmp(crypt(password, salt), stored_hash))
      return 1;
    else
      return 0;
  } else {
    printf("Failed to extract hash!\n");
    regfree(&hash_match);
    return 0;
  }
}

/**
 * get_password
 *
 * @param buf A pointer to the buffer string to store the retrieved password in
 *
 * @return 0 on failure, 1 on success
 */
int get_password(char *buf) {
  // For turning off terminal echoing
  struct termios old, new;
  
  printf("[mysudo] password for %s: ", getpwuid(getuid())->pw_name);
  fflush(stdout);

  // Turn echoing off
  /*
  if (tcgetattr(STDIN_FILENO, &old) != 0) {
    printf("Failed to get STDIN attributes\n");
    return 0;
  }
  new = old;
  new.c_lflag &= ~ECHO;
  if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &new) != 0) {
    printf("Failed to disable echoing\n");
    return 0;
  }
  */

  // Look at how cool I am using a fancy pointer solution instead of a
  // bounds-checking index solution
  while ((*buf = fgetc(stdin)) != '\n')
    buf++;
  *buf = 0;

  /*
  // Turn echoing back on
  if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &old) != 0) {
    printf("Failed to re-enable echoing\n");
    return 0;
  }
  */

  // Add a padding newline
  printf("\n");
 
  // No errors reported, we can safely say we've successfully retrieved the password
  return 1;
}

/**
 * check_auth
 *
 * @return 0 on authentication failure, 1 on authentication success
 */
int check_auth() {
  char saved_hash[128];
  char password_buf[1024];

  // Load the stored hash into saved_hash
  if (!get_stored_hash(saved_hash)) {
    printf("Failed to get hash stored in /etc/shadow!");
    return 0;
  }

  // Load the password into buf
  if (!get_password(password_buf)) {
    printf("Failed to get password from stdin!\n");
    return 0;
  }
 
  // Authenticate our password against the saved hash
  return authenticate(password_buf, saved_hash);
}

/**
 * Spawn subprocess
 *
 * @param argc The number of arguments given to mysudo
 * @param argv The list of arguments given to mysudo
 *
 * @return Exit status of the subprocess, -1 on failure
 */
int subprocess(int argc, char **argv) {
  pid_t pid;
  int status;
  FILE *pout = NULL;
  char buf[1024];
  char buf2[1024];
  char *tmp;

  // If there are no arguments simply return
  if (!argc) {
    printf("No command given\n");
    return -1;
  }

  // Generate the string we will use to determine the full patch of a command
  sprintf(buf2, "%s -c \"which %s\"", getenv("SHELL"), argv[0]);

  // Use popen to get the absolute path of our command
  if ((pout = popen(buf2, "r")) == NULL) {
    printf("Failed to find the path of %s\n", argv[0]);
    return -1;
  }

  // Get the full path from our subprocess
  tmp = buf;
  while ((*tmp = fgetc(pout)) != '\n')
    tmp++;
  *tmp = 0;

  // Kill le process
  pclose(pout);

  // Spawn a subprocess running the given command (argv[0])
  // Ensure that the entire argv for the new process is given, including the self-referencing argv[0]
  if ((status = posix_spawn(&pid, buf, NULL, NULL, argv, environ)) == 0) {
    // Wait on the child to exit
    if (waitpid(pid, &status, 0) == -1) {
      printf("Could not wait on child!\n");
      return -1;
    }
  }

  return status;
}

// Awesome authenticator
int main(int argc, char **argv) {
  if (check_auth()) {
    printf("Authenticated\n");
    
    // Elevate privileges if properly authenticated
    setuid(0);

    // Return the exit status of the called process
    // Adding 1 to argv to advance the pointer to where the self-referencing argv[0] is left out
    return subprocess(argc - 1, argv + 1);
  } else {
    printf("Authentication failed\n");
    return -1;
  }
}
