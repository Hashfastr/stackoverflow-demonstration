all: build

build: main.c
	gcc -fno-stack-protector -g main.c -lcrypt -o mysudo
	sudo chown root:root ./mysudo
	sudo chmod '4755' ./mysudo

clean:
	rm mysudo
